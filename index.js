// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:

const gradingSystem = (grade) =>{
	let msg = `Your quarterly average is ${grade}. You have received`;

	return grade <= 74 ? `Sorry! ${msg} Failed mark`
		: grade <= 80 ? `Congratulation! ${msg} a Beginner mark.`
		: grade <= 85 ? `Congratulation! ${msg} a Developing mark.`
		: grade <= 90 ? `Congratulation! ${msg} an Above average mark.`
		: grade <= 100 ? `Congratulation! ${msg} an Advanced mark.`
		: `Sorry! ${grade} Grade cannot be process!`
}
console.log(gradingSystem(91));

/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even

		5 - odd
		etc.
*/

// Code here:
const showOddEven = (num)=> {
	for(let i = 1; i <= num; i++){
	 i % 2 == 0 ? console.log(`${i} - even`) : console.log(`${i} - odd`);	
	}
};
showOddEven(10);

/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/

const hero= {
	heroName : "",
	origin : "",
	description : "",
	skill : {
		skill1 : "",
		skill2 : "",
		skill3 : ""
	}
}
hero.heroName = prompt("Enter Hero Name:");
hero.origin = prompt("Enter Origin:");
hero.description = prompt("Enter a Description:");
hero.skill.skill1 = prompt("Enter First Skill:");
hero.skill.skill2 = prompt("Enter Second Skill:");
hero.skill.skill3 = prompt("Enter Third Skill:");

console.log(hero);
console.log(JSON.stringify(hero));
